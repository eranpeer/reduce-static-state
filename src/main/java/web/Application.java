package web;
import devcraft.core.IndexLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws IOException {
        IndexLoader indexLoader = new IndexLoader();
        indexLoader.load();
        SpringApplication.run(Application.class, args);
    }

}