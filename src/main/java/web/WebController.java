package web;

import devcraft.core.ProductIndex;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class WebController {

    @RequestMapping("/category")
    public List<ProductIndex.Category> all() throws IOException {
        return ProductIndex.fetchAll();
    }


    @RequestMapping("/category/{id}")
    public ProductIndex.Category byId(@PathVariable("id") int id) throws IOException {
        return ProductIndex.fetch(new ProductIndex.CategoryKey(id));
    }

}

