package devcraft.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;

public class FastCache {

	private static Path dataFolder;
	private static int maxObjectsInMem = 100;
	private static Random random = new Random(System.currentTimeMillis());
	private static LinkedHashMap<String, String> loadedValues = new LinkedHashMap<>();

	static {
		maxObjectsInMem = Integer.valueOf(System.getProperty("maxObjectsInMem", "100"));
		initDataFolder();
	}

	private static void initDataFolder() {
		try {
			dataFolder = Files.createTempDirectory(Paths.get("."), "categoryMetadata");
			getKeysFolder().mkdirs();
			getDataFolder().mkdirs();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized static String get(String key) throws IOException {
		String value = loadedValues.get(key);
		if (value == null) {
			value = loadValueFromDisk(key);
		}
		return value;
	}

	public synchronized static void store(String key, String value) throws IOException {
		if (value == null)
			throw new IllegalArgumentException("value can't be null");
		updateDiskStore(key, value);
		updateLoadedValues(key, value);
	}

	public static synchronized Object remove(String key) throws IOException {
		try {
			return deleteValueFromDisk(key);
		} finally {
			loadedValues.remove(key);
		}
	}

	public static synchronized void delete() throws IOException {
		FileUtils.cleanDirectory(getKeysFolder());
		FileUtils.cleanDirectory(getDataFolder());
		loadedValues.clear();
	}

	public static synchronized List<String> allValues() throws IOException {
		ArrayList<String> result = new ArrayList<String>();
		for (File f : getDataFolder().listFiles()) {
			result.add(readDataFile(f));
		}
		return result;
	}

	private static File getDataFolder() {
		return new File(dataFolder.toFile(), "data");
	}

	private static File getKeysFolder() {
		return new File(dataFolder.toFile(), "keys");
	}

	private static void updateDiskStore(String key, String value) throws IOException {
		File keyFile = getKeyFile(key);
		File valueFile = null;
		if (keyFile.createNewFile()) {
			valueFile = makeFileForValue(getDataFolder());
		} else {
			valueFile = locateDataFile(keyFile);
		}
		FileUtils.write(valueFile, value, Charset.forName("UTF-8"), false);
		FileUtils.write(keyFile, valueFile.getName(), Charset.forName("UTF-8"), false);
	}

	private static File locateDataFile(File keyFile) throws IOException {
		try {
			String valueFilePath = readDataFile(keyFile);
			return new File(getDataFolder(), valueFilePath);
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	private static String loadValueFromDisk(String key) throws IOException {
		File dataFile = locateDataFile(getKeyFile(key));
		if (dataFile == null)
			return null;
		String value = readDataFile(dataFile);
		updateLoadedValues(key, value);
		return value;
	}

	private static String readDataFile(File dataFile) throws IOException {
		return FileUtils.readFileToString(dataFile, Charset.forName("UTF-8"));
	}

	private static File getKeyFile(String key) {
		return new File(getKeysFolder(), key);
	}

	private static void updateLoadedValues(String key, String value) {
		loadedValues.put(key, value);
		removeOverloadedObjects(maxObjectsInMem);
	}

	private static void removeOverloadedObjects(int maxObjectsInMem) {
		while (loadedValues.size() > maxObjectsInMem) {
			Iterator<Map.Entry<String, String>> iterator = loadedValues.entrySet().iterator();
			iterator.next();
			iterator.remove();
		}
	}

	private static File makeFileForValue(File parent) throws IOException {
		File valueFile = randomFileName(parent);
		while (!valueFile.createNewFile()) {
			valueFile = randomFileName(parent);
		}
		return valueFile;
	}

	private static File randomFileName(File parent) {
		String fileName = fileName(random.nextLong());
		return new File(parent, fileName);
	}

	private static String fileName(long num) {
		return String.valueOf(num);
	}

	private static Object deleteValueFromDisk(String key) throws IOException {
		File keyFile = getKeyFile(key);
		File dataFile = locateDataFile(keyFile);
		keyFile.delete();
		if (dataFile == null)
			return null;
		String value = readDataFile(dataFile);
		dataFile.delete();
		return value;
	}

}