package devcraft.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class ProductIndex {

    public static class CategoryKey {
        private int itemId;

        public CategoryKey(int itemId) {
            this.itemId = itemId;
        }
    }

    public static class Category {

        private int id;
        private String name;
        private int parentId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }
    }

    private static ObjectMapper mapper = new ObjectMapper();

    public static Category[] fetchPath(CategoryKey catalogKey) throws IOException {
        Category category = fetch(catalogKey);
        if (category == null)
            return null;
        return fetchCategoryPath(category);
    }

    public static void delete() throws IOException {
        FastCache.delete();
    }

    public static void store(CategoryKey catalogKey, Category category) throws IOException {
        FastCache.store(toStringkey(catalogKey), toJsonString(category));
    }

    public static Category fetch(CategoryKey catalogKey) throws IOException {
        String json = FastCache.get(toStringkey(catalogKey));
        if (json == null)
            return null;
        return toCategory(json);
    }

    private static Category toCategory(String json) throws IOException {
        return mapper.readValue(json, Category.class);
    }

    public static List<Category> fetchAll() throws IOException {
        List<Category> result = new ArrayList<>();
        List<String> jsons = FastCache.allValues();
        for (String json : jsons) {
            result.add(toCategory(json));
        }
        return result;
    }

    private static Category[] fetchCategoryPath(Category category) throws IOException {
        ArrayDeque<Category> queue = new ArrayDeque<>(1);
        queue.add(category);
        while (category.getParentId() != 0) {
            category = fetch(new CategoryKey(category.getParentId()));
            queue.addFirst(category);
        }
        return queue.toArray(new Category[queue.size()]);
    }

    private static String toJsonString(Category category) throws JsonProcessingException {
        return mapper.writeValueAsString(category);
    }

    private static String toStringkey(CategoryKey catalogKey) {
        return String.valueOf(catalogKey.itemId);
    }

}