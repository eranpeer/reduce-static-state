package devcraft.core;

import java.io.IOException;
import java.util.Arrays;

public class IndexLoader {

    private static String dataFile  ="someFilePath";

    public void load() throws IOException {
        for (ProductIndex.Category c : readCategories(dataFile)) {
            ProductIndex.store(new ProductIndex.CategoryKey(c.getId()), c);
        }
    }

    private Iterable<ProductIndex.Category> readCategories(String dataFile) {
        ProductIndex.Category c1 = new ProductIndex.Category();
        ProductIndex.Category c2 = new ProductIndex.Category();
        ProductIndex.Category c3 = new ProductIndex.Category();
        ProductIndex.Category c4 = new ProductIndex.Category();
        ProductIndex.Category c5 = new ProductIndex.Category();

        c1.setId(1);
        c2.setId(2);
        c3.setId(3);
        c4.setId(4);
        c5.setId(5);

        c2.setParentId(1);
        c3.setParentId(1);
        c4.setParentId(2);
        c5.setParentId(2);

        return Arrays.asList(c1, c2, c3, c4, c5);
    }
}