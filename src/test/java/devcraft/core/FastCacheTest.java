package devcraft.core;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
public class FastCacheTest {

    @Before
    public void init() throws IOException {
    }

    @After
    public void cleanup() throws IOException {
        FastCache.delete();
    }

    @Test
    public void shouldReturnNullIfKeyNotFound() throws IOException {
        Assert.assertThat(FastCache.get("some key"), is(nullValue()));
    }

    @Test
    public void shouldReturnStoredObejctIfObjectsExists() throws IOException {
        FastCache.store("key1", "value1");
        Assert.assertThat(FastCache.get("key1"), is("value1"));
    }

    @Test
    public void shouldReturnNullAfterRemovingAnExistingObject() throws IOException {
        FastCache.store("key1", "value1");
        Assert.assertThat(FastCache.get("key1"), is("value1"));
        FastCache.remove("key1");
        Assert.assertThat(FastCache.get("key1"), is(nullValue()));
    }

    @Test
    public void removeShouldReturnNullIfObjectDoesNotExists() throws IOException {
        FastCache.store("key1", "value1");
        Assert.assertThat(FastCache.get("key1"), is("value1"));
        Assert.assertThat(FastCache.remove("key1"),is("value1"));
    }

    @Test
    public void shouldUpdateExistingValueOnDisk() throws IOException {
        FastCache.store("key1", "value1");
        FastCache.store("key1", "value2");
        Assert.assertThat(FastCache.get("key1"), is("value2"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void shouldRejectNullValue() throws IOException {
        FastCache.store("key1", null);
    }

    @Test
    public void shouldReturnAllValues() throws IOException {
        FastCache.store("key1", "value1");
        FastCache.store("key2", "value2");

        Assert.assertTrue(FastCache.allValues().containsAll(Arrays.asList("value1","value2")));
    }

}