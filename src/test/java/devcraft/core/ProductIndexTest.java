package devcraft.core;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class ProductIndexTest {

    private ProductIndex.CategoryKey category1Key = new ProductIndex.CategoryKey(1);
    private ProductIndex.CategoryKey category2Key = new ProductIndex.CategoryKey(2);
    private ProductIndex.Category category1 = new ProductIndex.Category();
    private ProductIndex.Category category2 = new ProductIndex.Category();

    public ProductIndexTest() throws IOException {
        category1.setId(1);
        category1.setName("category1");
        category2.setId(2);
        category2.setName("category2");
        category2.setParentId(1);
    }

    @After
    public void  cleanup() throws IOException {
        ProductIndex.delete();
    }
    
    @Test
    public void shouldReturnNullIfCategoryDoesNotExist() throws IOException {
        Assert.assertNull(ProductIndex.fetch(category1Key));
    }

    @Test
    public void shouldReturnCategoryIfCatrgoryExists() throws IOException {
        ProductIndex.store(category1Key, category1);
        assertEqualValues(this.category1, ProductIndex.fetch(category1Key));
    }

    @Test
    public void shouldReturnCategoryPathOnFetchPath() throws IOException {
        ProductIndex.store(category1Key, category1);
        ProductIndex.store(category2Key, category2);
        ProductIndex.Category[] path = ProductIndex.fetchPath(category2Key);
        assertEqualValues(this.category1, path[0]);
        assertEqualValues(this.category2, path[1]);
    }

    @Test
    public void shouldReturnNullPathIfCategoryDoesNotExist() throws IOException {
        ProductIndex.Category[] path = ProductIndex.fetchPath(category1Key);
        Assert.assertNull(path);
    }

    private void assertEqualValues(ProductIndex.Category c1, ProductIndex.Category c2) {
        Assert.assertEquals(c1.getId(), c2.getId());
        Assert.assertEquals(c1.getName(), c2.getName());
    }

}